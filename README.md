# AlienAware

#### Game description:

Shooting aliens and stuff.

---

# TODOs

* Submenus (Options, etc)
* Normal game mode
* Gun types
* Enemy types
* Ground

##### TypeNShoot Mode

* Another type of enemy
* Ground
* Better UI
* FPS info
* Typing speed (characters per second)
* Optimalize enemy spawn time

##### Already done

* Created project
* Main Menu
* Pause Menu

---

# License
MIT

**Yay, free stuff :).**