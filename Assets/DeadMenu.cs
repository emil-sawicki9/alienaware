﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeadMenu : Menu {

  public Text scoreText;

  public void Apply()
  {
    IsOpen = false;
  }

  public void Cancel()
  {
    IsOpen = false;
  }

  protected override void BeforeOpen()
  {
    scoreText.text = GameManager.instance.Score.ToString();
  }
}
