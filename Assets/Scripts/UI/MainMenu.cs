﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

  public AudioClip clickClip;

	void Update () {
    if (Input.GetMouseButtonDown(0))
      SoundManager.instance.PlaySingle(clickClip);
	}
}
