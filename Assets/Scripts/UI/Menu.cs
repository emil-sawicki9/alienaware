﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
  [HideInInspector]
  public Menu currentSubmenu = null;

  private Animator _animator;
  private CanvasGroup _canvasGroup;

  public bool IsOpen
  {
    get { return _animator.GetBool("IsOpen"); }
    set { BeforeOpen();  _animator.SetBool("IsOpen", value); }
  }

  public bool IsSubmenuOpen
  {
    get { return _animator.GetBool("IsSubmenuOpen"); }
    set { _animator.SetBool("IsSubmenuOpen", value); }
  }

  void Awake()
  {
    _animator = GetComponent<Animator>();
    _canvasGroup = GetComponent<CanvasGroup>();

    var rect = GetComponent<RectTransform>();
    rect.offsetMin = rect.offsetMax = new Vector2(0, 0);
  }

  void Update()
  {
    _canvasGroup.blocksRaycasts = _canvasGroup.interactable = _animator.GetCurrentAnimatorStateInfo(0).IsName("Open");

    if (currentSubmenu != null)
    {
      if ((Input.GetKeyDown(KeyCode.Escape) && currentSubmenu.IsOpen) || !currentSubmenu.IsOpen)
      {
        currentSubmenu.IsOpen = false;
        IsSubmenuOpen = false;
        currentSubmenu = null;
      }
    }
  }

  virtual protected void BeforeOpen()
  {

  }
}