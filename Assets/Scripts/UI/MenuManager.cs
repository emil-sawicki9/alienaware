﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

  public Menu currentMenu;

	void Start () {
    if (currentMenu != null)
      ShowMenu(currentMenu);
	}
	
	public void ShowMenu(Menu menu)
  {
    if (currentMenu != null)
      currentMenu.IsOpen = false;

    currentMenu = menu;
    currentMenu.IsOpen = true;
  }

  public void HideMenu()
  {
    if (currentMenu != null)
      currentMenu.IsOpen = false;
  }

  public void ShowSubmenu(Menu menu)
  {
    menu.IsOpen = true;
    currentMenu.currentSubmenu = menu;
    currentMenu.IsSubmenuOpen = true;
  }

  public void ShowScene(string name)
  {
    SceneManager.LoadScene(name);
  }

  public void Exit()
  {
    Application.Quit();
  }
}
