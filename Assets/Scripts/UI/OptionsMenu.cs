﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : Menu {

  public void Apply()
  {
    IsOpen = false;
  }

  public void Cancel()
  {
    IsOpen = false;
  }

  protected override void BeforeOpen()
  {
    
  }
}
