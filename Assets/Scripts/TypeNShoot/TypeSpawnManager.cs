﻿using UnityEngine;
using System.Collections;

public class TypeSpawnManager : SpawnManager {

  public float enemyCooldownLeft = 10.0f;
  public float enemyCooldownRight = 10.0f;

  private float timeLeftSpawn;
  private float timeRightSpawn;

  private EnemySpawner enemySpawnerL = null;
  private EnemySpawner enemySpawnerR = null;

  void Start () {
    enemySpawnerL = GameObject.Find("EnemySpawnerLeft").GetComponent<EnemySpawner>();
    enemySpawnerR = GameObject.Find("EnemySpawnerRight").GetComponent<EnemySpawner>();


    //timeLeftSpawn = enemyCooldownLeft + 0.1f;
    //timeRightSpawn = enemyCooldownRight + 0.1f;
  }

  public override void SpawnEnemy()
  {
    if (timeLeftSpawn > enemyCooldownLeft)
    {
      enemySpawnerL.spawnEnemy();
      timeLeftSpawn = 0.0f;
    }
    else
      timeLeftSpawn += Time.deltaTime;

    if (timeRightSpawn > enemyCooldownRight)
    {
      enemySpawnerR.spawnEnemy();
      timeRightSpawn = 0.0f;
    }
    else
      timeRightSpawn += Time.deltaTime;
  }
}
