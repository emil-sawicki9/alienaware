﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {

  public enum SpawnSide { Left, Right };

  public SpawnSide spawnSide;
  public GameObject enemyPrefab;
  public GameObject followNamePrefab;
  public float difficultySpikeCD = 10.0f;

  private Vector3 pos, size;
  private float distFromCamera = 0.5f;
  private float lastCameraSize = 0.0f;
  private string[] names;
  private float gameTime = 0.0f;

	void Start () {
    size = GetComponent<Renderer>().bounds.size;
    pos = gameObject.transform.position;
    TextAsset ta = Resources.Load("name") as TextAsset;
    names = ta.text.Split('\n');
    for (int i = 0; i < names.Length; i++)
      names[i] = names[i].Replace('\n', ' ').Trim();

  }
	
	void Update () {
    if (!GameManager.instance.playerDead && !GameManager.instance.pause)
    {
      if (lastCameraSize != Camera.main.orthographicSize * Camera.main.aspect)
      {
        float width = 2f * Camera.main.orthographicSize * Camera.main.aspect;
        pos = gameObject.transform.position;
        if (spawnSide == SpawnSide.Left) // LEFT spawner
        {
          if (pos.x != -width / 2 - distFromCamera)
            transform.position = new Vector3(-width / 2 - distFromCamera, pos.y, pos.z);
        }
        else // RIGHT spawner
        {
          if (pos.x != width / 2 + distFromCamera)
            transform.position = new Vector3(width / 2 + distFromCamera, pos.y, pos.z);
        }
        pos = gameObject.transform.position;
        lastCameraSize = Camera.main.orthographicSize * Camera.main.aspect;
      }
    }
    gameTime += Time.deltaTime;
	}

  public void spawnEnemy()
  {
    GameObject obj = (GameObject)Instantiate(enemyPrefab, new Vector3(pos.x, Random.Range(pos.y + size.y/2, pos.y - size.y/2), pos.z), Quaternion.identity);
    string name = "";
    float diff = gameTime / difficultySpikeCD;
    if (diff < 1)
      diff = 1;
    for (int i = 0; i < diff; i++)
      name += names[Random.Range(0, names.Length - 1)];
    Enemy e = obj.GetComponent<Enemy>();
    e.enemyName = name;
    GameObject nameTextObj = (GameObject)Instantiate(followNamePrefab, e.gameObject.transform.position, Quaternion.identity);
    nameTextObj.GetComponent<FollowingName>().target = e;

    e.additonalObjects.Add(nameTextObj);
  }
}
