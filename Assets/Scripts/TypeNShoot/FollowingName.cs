﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FollowingName : MonoBehaviour {

  [HideInInspector]
  public Enemy target = null;
  [HideInInspector]
  public Text text;

	void Start () {
    text = gameObject.GetComponent<Text>();
    gameObject.transform.SetParent(GameObject.Find("Canvas").transform);
    text.text = target.enemyName;
    target.enemyName = text.text;
	}

  void Update()
  {
    if (!GameManager.instance.playerDead && !GameManager.instance.pause)
    {
      if (target != null)
      {
        Vector2 targetPos = Camera.main.WorldToScreenPoint(target.transform.position + new Vector3(0, -target.height));
        transform.position = targetPos;
      }
    }
  }

}
