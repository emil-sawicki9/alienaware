﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TypeText : MonoBehaviour {

  private InputField inputField;

  void Start () {
    inputField = gameObject.GetComponent<InputField>();
	}
	
	void Update ()
  {
    inputField.interactable = true;
    inputField.readOnly = false;
    if (GameManager.instance.playerDead || GameManager.instance.pause)
    {
      inputField.interactable = false;
      inputField.readOnly = true;
    }
    else if (inputField.isFocused == false)
    {
      EventSystem.current.SetSelectedGameObject(inputField.gameObject, null);
      inputField.OnPointerClick(new PointerEventData(EventSystem.current));
    }
  }

  public void onSubmit()
  {
    GameManager.instance.shootTarget(inputField.text);
    inputField.text = "";
  }
}
