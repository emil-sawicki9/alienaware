﻿using UnityEngine;
using System.Collections;

public class Pistol : Gun {

  public override void shoot()
  {
    Debug.Log("SHOOT!");
    particle.Emit(1);
    base.shoot();
  }
}
