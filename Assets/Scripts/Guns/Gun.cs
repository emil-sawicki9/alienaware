﻿using UnityEngine;
using System.Collections;

public abstract class Gun : MonoBehaviour {

  public AudioClip sound;

  protected ParticleSystem particle;

  void Awake()
  {
    particle = GetComponent<ParticleSystem>();
  }

  public virtual void shoot()
  {
    SoundManager.instance.PlaySingle(sound);
  }
}
