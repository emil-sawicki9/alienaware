﻿using UnityEngine;
using System.Collections;

public class Shotgun : Gun {

  public override void shoot()
  {
    particle.Emit(5);
    base.shoot();
  }
}
