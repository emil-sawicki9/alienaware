﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {

  public float Speed = 0.1f;

  [HideInInspector]
  public string enemyName = "";
  [HideInInspector]
  public float height = 0.0f;

  [HideInInspector]
  public List<GameObject> additonalObjects = new List<GameObject>();

	void Start () {
    GameManager.instance.AddEnemyToList(this);
    height = gameObject.GetComponent<Renderer>().bounds.size.y;
  }

  void Update () {
    if (!GameManager.instance.playerDead && !GameManager.instance.pause)
      transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, Speed * Time.deltaTime);
  }

  void OnTriggerEnter2D(Collider2D coll)
  {
    if (coll.gameObject.tag == "Player")
    {
      GameManager.instance.die();
    }
  }

  void OnParticleCollision(GameObject other)
  {
    Destroy(gameObject);
    foreach (GameObject obj in additonalObjects)
      Destroy(obj);
    GameManager.instance.addPoint(this);
  }
}
