﻿using UnityEngine;
using System;
using System.Collections.Generic; 	
using Random = UnityEngine.Random; 	

public class BoardManager : MonoBehaviour
{
  [Serializable]
  public class Array2d
  {
    public GameObject[] array;
  }
  public int columns = 64;                   
  public int rows = 64;                 
  public Array2d[] floorTiles;

  public Vector3 startingPos;

  private Transform boardHolder;                
  private List<Vector3> gridPositions = new List<Vector3>(); 

  void InitialiseList()
  {
    gridPositions.Clear();

    for (int x = 1; x < columns - 1; x++)
    {
      for (int y = 1; y < rows - 1; y++)
      {
        gridPositions.Add(new Vector3(x, y, 0f));
      }
    }
  }

  void BoardSetup()
  {
    //boardHolder = new GameObject("Board").transform;
    int groundTypeIdx = Random.Range(0, floorTiles.Length);
    for (int x = 0; x < columns ; x++)
    {
      for (int y = 0; y < rows; y++)
      {
        GameObject toInstantiate = floorTiles[groundTypeIdx].array[Random.Range(0, floorTiles[groundTypeIdx].array.Length)];
        Instantiate(toInstantiate, new Vector3(startingPos.x + x * 0.3f, startingPos.y + y * 0.3f, startingPos.z), Quaternion.identity);
        //GameObject instance = Instantiate(toInstantiate, new Vector3(startingPos.x + x * 0.3f, startingPos.y + y * 0.3f, startingPos.z), Quaternion.identity) as GameObject;
        //instance.transform.SetParent(boardHolder);
      }
    }
  }


  Vector3 RandomPosition()
  {
    int randomIndex = Random.Range(0, gridPositions.Count);
    Vector3 randomPosition = gridPositions[randomIndex];
    gridPositions.RemoveAt(randomIndex);
    return randomPosition;
  }

  public void SetupScene()
  {
    BoardSetup();
    InitialiseList();
  }
}
