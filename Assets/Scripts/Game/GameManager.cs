﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

  public GameObject spawnPrefab;

  public static GameManager instance = null;

  public BoardManager boardManager;

#if UNITY_EDITOR
  public bool showBackground = true;
  public bool spawnEnemies = true;
#endif 

  public Menu pauseMenu = null;
  public Menu deadWindow = null;
  [HideInInspector]
  public bool playerDead = false;
  [HideInInspector]
  public bool pause = false;
  public int Score {
    get { return score; }
  }

  private Text scoreText = null;
  private Text enemyCountText = null;
  private Player player = null;
  private MenuManager menuManager;
  private Camera menuCamera;
  private Camera mainCamera;
  private Canvas mainCanvas;
  private Canvas menuCanvas;

  private int score = 0;

  private List<Enemy> enemies = new List<Enemy>();

  void Awake()
  {
    if (instance == null)
      instance = this;
    else if (instance != this)
      Destroy(gameObject);

    player = GameObject.Find("player").GetComponent<Player>();
    scoreText = GameObject.Find("Canvas/UIContainer/score/scoreCount").GetComponent<Text>();
    enemyCountText = GameObject.Find("Canvas/UIContainer/enemy/enemyCount").GetComponent<Text>();
    menuManager = gameObject.GetComponent<MenuManager>();
    mainCamera = Camera.main;
    mainCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
    menuCamera = GameObject.Find("Menu Camera").GetComponent<Camera>();
    menuCanvas = GameObject.Find("MenuCanvas").GetComponent<Canvas>();


#if UNITY_EDITOR
    if (spawnEnemies)
      Instantiate(spawnPrefab, transform.position, Quaternion.identity);
    if (showBackground)
      boardManager.SetupScene();
#else
    Instantiate(spawnPrefab, transform.position, Quaternion.identity);
    //boardManager.SetupScene();
#endif

  }

  void Update()
  {
    if (!playerDead)
    {
      if (Input.GetKeyDown(KeyCode.Escape) && pauseMenu != null)
      {
        if (pauseMenu.IsOpen)
        {
          menuManager.HideMenu();
          toggleMenu();
          pause = false;
        }
        else
        {
          pause = true;
          toggleMenu();
          menuManager.ShowMenu(pauseMenu);
        }
      }
    }
  }

  private void toggleMenu()
  {
    bool c = mainCamera.enabled;
    mainCamera.enabled = !c;
    menuCamera.enabled = c;
    mainCanvas.gameObject.SetActive(!c);
    menuCanvas.gameObject.SetActive(c);
  }

  public void shootTarget(string name)
  {
    name = name.ToLower();
    List<Enemy> targetEnemies = new List<Enemy>();
    foreach (Enemy e in enemies)
    {
      if (e.enemyName == name)
        targetEnemies.Add(e);
    }

    if (targetEnemies.Count > 0)
    {
      targetEnemies.Sort(SortByDistance);
      player.shoot(targetEnemies[0]);
    }
  }

  public void addPoint(Enemy enemy)
  {
    enemies.Remove(enemy);
    score++;
    enemyCountText.text = enemies.Count.ToString();
    scoreText.text = score.ToString();
  }

  public void AddEnemyToList(Enemy e)
  {
    enemies.Add(e);
    enemyCountText.text = enemies.Count.ToString();
  }

  public int SortByDistance(Enemy a, Enemy b)
  {
    var dstToA = Vector3.Distance(transform.position, a.transform.position);
    var dstToB = Vector3.Distance(transform.position, b.transform.position);
    return dstToA.CompareTo(dstToB);
  }

  public void die()
  {
    playerDead = true;
    toggleMenu();
    menuManager.ShowMenu(deadWindow);
  }

  public void restartGame()
  {
    menuManager.ShowScene("TypeNShoot");
  }
}

