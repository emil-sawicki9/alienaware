﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour {

  public AudioClip shootAudio;

  public ParticleSystem particle;

  private Gun gun;

  void Start () {
    gun = GetComponentInChildren<Gun>();
  }

  void Update()
  {
    gun.gameObject.transform.position = transform.position;
    if (!GameManager.instance.playerDead && !GameManager.instance.pause)
    {

    }

#if UNITY_EDITOR
    if (!GameManager.instance.spawnEnemies)
    {
      //rotate to mouse
      float angle = Mathf.Atan2(Camera.main.ScreenToWorldPoint(Input.mousePosition).y, Camera.main.ScreenToWorldPoint(Input.mousePosition).x) * Mathf.Rad2Deg;
      transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

      if (Input.GetMouseButtonDown(0))
        gun.shoot();
    }
#endif
  }

  public void shoot(Enemy enemy)
  {
    float angle = Mathf.Atan2(enemy.transform.position.y, enemy.transform.position.x) * Mathf.Rad2Deg;
    transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    gun.shoot();   
  }
}
