﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {

  [HideInInspector]
  public SpawnManager instance = null;

  void Awake()
  {
    if (instance == null)
      instance = this;
    else if (instance != this)
      Destroy(gameObject);
  }

  void Start () {
	
	}
	
	public void Update()
  {
    if (!GameManager.instance.playerDead && !GameManager.instance.pause)
      SpawnEnemy();
	}

  public virtual void SpawnEnemy()
  {

  }
}
