﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

  public AudioSource efxSource;         
  public AudioSource musicSource;         
  public static SoundManager instance = null;
  public float lowPitchRange = .95f;       
  public float highPitchRange = 1.05f;     

  void Awake()
  {
    if (instance == null)
      instance = this;
    else if (instance != this)
      Destroy(gameObject);

    musicSource.loop = true;
  }

  public void PlaySingle(AudioClip clip)
  {
    efxSource.PlayOneShot(clip);
  }

  public void RandomizeSfx(params AudioClip[] clips)
  {
    efxSource.pitch = Random.Range(lowPitchRange, highPitchRange);
    efxSource.PlayOneShot(clips[Random.Range(0, clips.Length)]);
  }
}
